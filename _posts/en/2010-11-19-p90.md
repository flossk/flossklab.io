---
layout: post
lang: en
category: en
title: 'Fedora 14 Release Party'
author: "admin"
---
Fedora 14 Laughlin Release in Kosovo was again celebrated by FLOSSK with a short presentation on the FedoraProject community and the innovative projects part of the new version Fedora 14 Laughlin.

The event took place at FLOSSK's hacklab, where although there was no projector, a presentation was done in one of the monitors. The interest of people present was quite high, and although CDs couldn't be handed out, coffee and tea was :).

Attendees left quite happy with the event.

More about the FedoraProject: https://fedoraproject.org/ https://fedoraproject.org/wiki/ http://en.wikipedia.org/wiki/Fedora_%28operating_system%29 https://fedoraproject.org/wiki/Releases/14/FeatureList
