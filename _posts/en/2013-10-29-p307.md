---
layout: post
lang: en
category: en
title: 'CryptoParty'
author: "ardian"
---
Attend a CryptoParty to learn and teach how to use basic cryptography tools.

A CryptoParty is free, public and fun. People bring their computers, mobile devices, and a willingness to learn!

CryptoParty is a decentralized, global initiative to introduce the most basic cryptography software and the fundamental concepts of their operation to the general public, such as the Tor anonymity network, public key encryption (PGP/GPG), and OTR (Off The Record messaging).

CryptoParties are free to attend, public, and commercially and politically non-aligned.

AGENDA:
1. Short intro what Cryptoparties are
2. Little bit of history
3. Showcase of open source tools such as (Tor, OTR GPG)
4. Bitcoin   the so called cryptocurrency
5. Time for the audience to experiment (Bring your laptops and smartphones)

-
Thank you ICK - Innovation Centre Kosovo﻿ for supporting this event
