---
layout: post
lang: en
category: en
title: 'Software Freedom Kosova 2010'
author: "admin"
---
![alt](/flossk.org/img/blog/1(1).jpg)
On 25 and 26 September “Software Freedom Kosova” Conference was hosted for the second time in Prishtina .SFK10 for the second time was organized by FLOSS Kosovo and the Faculty of Electrical Engineering and Computer (FIEK) of the University of Pristina.SFK09 held in 2009 was attended by about 500 people who attended about 40 lectures of 25 lecturers. Conference this time was more than last time and was more focused: there were 24 lectures from Kosovo, region and world.The main lecturers and also guest of honor of this conference were as renowned hacker

* Leon Shiman, board member of the Foundation that oversees the development of graphical system for Linux and BSD - x.org, and owner of Shiman Associates consulting firm
* Rob Savoye, the primary developer of Gnash as previously developed for Debian, Red Hat and Yahoo. Savoy codes since 1977
* Mikel Maron, OpenStreeMap Foundation board member
* Peter Salus, linguist, computer scientist and historian of technology.

SFK10 was prepared thanks to a job of almost a year from the organizing committee. Other topics were also offered by:

* Milot Shala
* Martin Bekkelund
* Baki Goxhaj
* Marco Fioretti

The conference was held at the premises of FIEK. Free Registration began Saturday at 09:00 and during the two days speeches began at 10:00. The conference was supported financially by the Office of the Prime Minister, Rrota, PC World Albania and OpenWorld.al

View the Photo Gallery of this event: FLOSSK GalleryFor more visit the official website of the conference: http://www.kosovasoftwarefreedom.org
