---
layout: post
lang: en
category: en
title: 'What can I do to help FLOSSK ?'
author: "mdupont"
---
Text of speech for SFD2011 Prizren

I would like to thank Besfort and Tarik for organizing the Software Freedom Day event in Prizren and inviting me to record a speech.

Having an event in prizren is important because it is an important city, and has many great people in it, and for the reason of creating more events outside of the capitol city of Prishtina. We need to create local groups, local centers of competence all around the region. Hopefully we will see more groups becoming active.

Local groups are important because each city has many people in it who are not connected to the events in Prishtina directly. Not many people have the resources to travel to Prishtina for meetings and events, many young people who could possibly be active members are lost if we are focused on one place.

Now, I would like to talk to you about something that is very important to me personally.

The biggest problem people have when they hear about FLOSSK is they don’t know what they can do.

The ask always, "What can I do to help?".


What can I do to help?

This is what I would like to answer today, what can YOU do, and how can YOU help.

First of all, you need to choose a project to work on, there so many of them.

You can see a list of projects that I have collected here and my letter that I wrote in 2010,http://en.wikipedia.org/wiki/User:Mdupont/OpenLetterToFreeSoftwareCommunities it is still valid.

There are projects like google summer of code where you will even be paid to work on these projects. http://code.google.com/soc/

The thing that is needed the most are software developers, and that is what our projects need.

Now if you are not a software developer, there are things you can do as well.
* You can take a copy of some interesting free/libre open source software package.
* You can translate the documentation and language strings to albanian, even to your local dialect.
* You can install the code on your computer and run it.
* You can test, find bugs and report them.

There are projects such as Wikipedia and Openstreetmap that are working on creating shared knowledge of the world.
* There is a connection between all these projects, they are based on the idea of sharing.
* Each project has a different license that regulates its sharing, there are different details, but the basic idea is that people work on these projects for the general public good. Instead of keeping it all to themselves, they share it. The idea is that when all people share, with the cost of sharing being almost zero, then all will benefit.
* You cannot share a car or a house easily, but sharing a copy of code or a map or an article does not cost!
* But if you are a software developer, you can read the code, and study it, you can change it and improve it.
* You can contact the authors, join the global community around that software or project.

Joining a free software community

Now joining the community is a very important part, and lets talk about that.

First of all, you need to choose the right forum. You need to be patient and you need to learn about the community itself, each project has its own rules, its own community, and each one is different.

But as about FLOSSK, the important thing is that we have productive members in different projects and this is important for Kosovo to help integrate it into the global community, and that is my goal.
1. IRC - internet relay chat.
	This is an old system that is used to talk in real time via chat across the world. For this you will need a client like xchat.  
	The most important thing with irc is that you will have to wait some times up to 24 hours for an answer.
	People are connected from the whole world and you will not always be on the same timezone. So you need to stay connected for days, and don’t leave. People are often frustrated and leave, you need to stick with it. There are often meetings at specified times. Some channels are so full that  it is hard to understand what is going on, there are many conversations going on at the same time.
	you can see some of the popular channels here http://irc.netsplit.de/channels/?net=freenode ( freenode.net ) or here http://irc.netsplit.de/channels/?net=oftc (oftc.net )
2. Mailing lists.
	Most projects have a mailing list, you need to subscribe to the list and start to read the messages. You will find an archive of older messages as well and you can read them to get answers.
3. Forums
	Many projects have some online forum where you can post questions and get answers, these are important for some projects and you can join up and get started.
4. Wikis
	Most projects have a wiki. You need to get an account there and read it. You can also start helping translate it to your language. Learning to edit wikis is an important skill that you should learn, it is worth the time and there are many many wikis around.
5. Bug trackers.
	Each project has a list of open bugs somewhere, this is a list of problems that need to be fixed, things to do. You can browse then and help fix them or add in your own reports.
6. Version control
	Then you will need to learn about SSH and public keys. In order to contribute code you will need to learn about how to use that. You will also need to learn about git, svn, bzr or other version control systems.
	And finally, you should work on your own blog. You should document what you are learning and doing and share it with the community.
You should be prepared to invest at least one hour a day and do that for at least one year. If you consistently invest time and energy into a project then you will reach the status of a beginner and be able to help represent FLOSSK in some project.
	It does not matter what your skills are, there are things you can do. You can help translate, you can create graphics, make videos, test programs, help organize meetings, or you can even help write code.
So, I hope that I have given you an overview of how to get started. Happy Software Freedom Day 2011.

	and here is also the video:

By: James Michael Dupont
