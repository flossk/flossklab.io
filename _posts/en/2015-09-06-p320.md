---
layout: post
lang: en
category: en
title: 'Prishtina Hackerspace'
author: "altin"
---
![alt](/flossk.org/img/blog/logo_long.png)

Prishtina Hackerspace is a co-working open experimentation space established exclusively for technological, educational, cultural and scientific purposes. Prishtina Hackerspace was initiated by FLOSSK members and operates within FLOSSK's umbrella as an NGO. The aim of the space is to:

* Provide workspace, equipment and other resources for communal use by all members;
* Encourage continued and after-school learning through workshops, classes, seminars and mentoring;
* Create a safe and open environment for experimentation in technology and art.

Visit the Prishtina Hackerspace webiste to learn more about the project and it's mission…

See also the FAQ here
