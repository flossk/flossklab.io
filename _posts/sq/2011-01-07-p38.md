---
layout: post
title: 'Parti i Lansimit të Drupal 7'
lang: sq
category: sq
author: "admin"
---
![alt](/flossk.org/img/blog/d7.jpg)
Më 7 Janar 2011 me rastin e lëshimit të versionit të ri të Drupal 7, nje CMS me licencë të hapur GPL (General Public License), është një ndër eventet tjera që FLOSSK dhë anëtarët e saj organizuan.

Bazuar në pikat  kryesore gjatë keti eventi u realizuan keto:

- Prezentimi i drupal 7, që u bë nga Gent Thaci anëtarë i FLOSSK-ut.
- Instalimi dhe testimi i Drupal 7 në një nga kompjuterët në zyrën e FLOSSK-ut, që u bë nga Altin Ukshini dhe u bë shperndarja e Stikerëve të Drupalit

Vizitojeni faqen ku u bë organizimi oficial i këti eventi për Drupal 7 dhe foto galerine për të parë më afër se qfarë ndodhi aty:http://groups.drupal.org/node/114709http://gallery.flossk.org/index.php?level=album&id=22

Për më shumë vizitojeni faqen zyrtare të Drupal: http://drupal.org/