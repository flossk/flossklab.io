---
layout: post
title: 'FLOSSTALK nga Betim Deva: Adaptimi i Sistemit Operativ Android'
lang: sq
category: sq
author: "altin"
---
![alt](/flossk.org/img/blog/android.jpg)
FLOSSTalk që tash e tutje pritet të jetë e zakonshme nga FLOSSK, të Enjten më datën 29.12.2011 solli Prishtinë një ligjeratë nga Betim Deva me temën, “Adaptimi i sistemit operativ Android”.
Betim Deva, i cili me profesion është programmer me përvojë nga Prishtina dhe me Master në Shkenca Kompjuterike nga Universitetit Shtetëror i Arizonas, ndajti eksperiencën e tij shum vjeqare me audiencën e pranishme prej rreth 30 pjesëmarrësve, ashtu siq tregon edhe titulli i ligjeratës Betimi foli për platformën Android dhe mbi mundësitë e saja për tu adoptuar në bazë të nevojave të klientit.
Me përvojën e tij si zhvillues i aplikacioneve me bazë në Android dhe të adoptuara për sistemin e Nook, lexuesin elektronik të Barnes & Noble, Betimi shpalosi disa detaje intersante mbi mënyrën e funksionimit të platformës, API’t e ndryshëm që gjenden në shfrytëzim, programimi i aplikacioneve duke shfrytëzuar ato, etj.
Takimi shkoi tamam ashtu siq edhe pritej ku pas ligjeratës të pranishmit patën rastin të jenë të hapur me pyetje dhe të diskutojnë edhe më gjerë në lidhje me temën e folur.
Menduam edhe për ata që nuk mundën të jenë pjesmarrës në këtë lighjeratë, gjat tërë kohës ligjerata ishte duke u shfaqur edhe live në ustream.tv ku përveq kësaj ju mund të shihni atë inqizim këtu.


Inqizimet tjera: [2] - [3]
Në vazhdim FLOSSTalk i radhës pritet të sjellë emër tjetër profesional megjithëse detajet ende nuk janë bërë të ditura.
	Për më shumë qëndroni të lidhur me Blogun tonë.
Dhe më poshtë mund ta shkarkoni prezantimin e Betim Devës si PDF file.
