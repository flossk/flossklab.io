---
layout: post
title: 'Wikivoyage Editathon held in Prishtina'
lang: sq
category: sq
author: "Anonim (e pa verifikuar)"
---
Volunteers, mostly first-time wiki editors, made contributions to Wikivoyage articles related to Kosovo on Sunday. Wikivoyage is the free travel guide that anyone can edit, a sister project of Wikipedia.

Fourteen contributors edited the Kosovo, Prizren, Gjakova, Peja and Mitrovica articles, along with articles about the neighboring countries that related to Kosovo. Participants proof-read, updated, illustrated, mapped and added content to articles. Some participants will continue to add their contributions from the work done on Sunday. One article was partially translated into French. 

More remains to be done. Better photos can be used to illustrate articles, some already available at Wikimedia Commons. Next week more work will be done in a separate event dedicated to Prizren.

Two key articles remain to be written using the Wikivoyage activity structure, on hiking and skiing in Kosovo, as well as improvement of articles about Gjilan, Ferizaj and Mitrovica. Once the Kosovo-related articles are written in English, they will be translated to major Wikivoyage languages: German, Italian, French, Spanish and Polish.

This activity was kindly sponsored by Buffalo Backpackers Hostel in Prishtina, Kosovo and the Wikimedia Foundation.

Thank you to all the contributors and see you at the next editathon.