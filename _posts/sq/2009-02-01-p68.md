---
layout: post
title: 'Kursi i GNU/Linux'
lang: sq
category: sq
author: "admin"
---
Anembanë botës ka ofrues të panumërt të trajnimeve me GNU/Linux, duhet theksuar se këtë e ka mbështetur mjaft shumë edhe organizata për Softuer të Lirë dhe të hapur FLOSSK , e cila ka kontribuar me kapacitetet e saja dhe me njohurin e saj për Softuerin e Lirë dhe të Hapur.

Në shkurt te vitit 2009 themelusi i OJQ FLOSSK James Michael Dupont filloi ta mbante një kurs për sistemin operativ GNU/Linux ketu në Prishtine në zyret e IDI-t (Information Development Initiative), kurs i cili ishte pa pagesë vetëm që të nxiste të rinjët në kosovë të përdornin dhe të mësonin rreth Softuerit te lirë.

Në ate kohë kishtë mjaft të interesuar për këtë kurs, por megjithatë pasi qe J.M.D iu desh të lëshonte Kosovën kursi u ndërpre, por ama u vazhduan në Qëshor të po atij viti nga disa studentë të tijë të cilët vazhduan punën më dhënien e ligjeratave për GNU/Linux sikurse edhe mësuesi i tyre.

Shpresojmë që edhe në të ardhmen të kemi mundësi materiale por edhe vullnetare që këto lloj kursesh ti vazhdojmë njëllojë ëdhë sot...
