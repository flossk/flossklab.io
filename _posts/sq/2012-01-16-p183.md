---
layout: post
title: 'FLOSSTalk : Kërkojmë folës për OpenStack'
lang: sq
category: sq
author: "altin"
---
![alt](/flossk.org/img/blog/openstack-logo512_125x125.png)
FLOSSK është në kërkim për folës të prezentojnë live apo nga distanca(remotely) në prani të audiencës  për temën openstack
FLOSSTalks janë fjalime të rregullta në lidhje me softuerin e lirë dhe të hapur që organizohen nga FLOSSK në Prishtinë, Kosovë.
	Ne jemi duke kërkuar për ekspertë nga e gjithë bota për të marrë pjesë në programin tonë të vazhdueshëm edukativ.
Folësit janë të ftuar që të vijnë në Prishtinë ose të dorëzojnë një video, të pregatisin një prezantim(slide) dhe të jenë në dispozicion për pyetje online nëpërmjet internetit nga audienca. Bisedimet duhet të jetë 45 minuta.
	Ne aktualisht jemi në kërkim për dikë që të flas për openstack dhe Open Source Cloud Computing.
Publiku ka përvojë në linux, por jo me openstack.
	Ne do të donim një bisedë hyrëse që paraqet temën dhe i motivon njerëzit që të inkuadrohen dhe të mësojnë rreth projektit.
FLOSSK gjithashtu organizon një konferencë vjetore "Software Freedom Kosova", e fundit ishte SFK'11 http://www.flossk.org/sq/konferenca/sfk11.
	Ne jemi në kërkim edhe për folësit në temën e openstack për konferencat e SFK në të ardhmen, ne zakonisht gjejmë sponsorë për shpenzimet e udhëtimit, por ju jeni të ftuar në përgjithësi që të vini kurdoherë të na vizitoni dhe të mbani dnojnë fjalim në prishtina kur të keni kohë.
Ju lutem kontaktoni [email protected] nëse jeni të interesuar,
