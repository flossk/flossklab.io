---
layout: post
title: 'Për programin qeverisës të Kosovës: Roli i FLOSSK-ut në zhvillimin e ekonomisë dhe pavarësisë teknologjike të Kosovës'
lang: sq
category: sq
author: "agron"
---
FLOSSK-u është një organizatë e përbërë nga profesionist dhe student të shkencave kompjuterike të cilët përkrahin zhvillimin e teknologjive me kod të hapur, vizion që dëshirojmë të adoptohet në Kosovë. 

**Problemi**

Duke e marrë parasysh se një përqindje e madhe e teknologjive kompjuterike krijohet jashtë vendit, Kosovës i duhet të importojë teknologji nga jashtë e cila shitet pa qasje në kod dhe mirëmbajtja e saj kërkon ekspertë nga vetë prodhuesi me kosto të lartë.

Kosova nuk ka ekonomi për të përballuar blerjen dhe mirëmbajtjen e teknologjive të importuara. P.sh. në rastin e Sistemit Informativ Shëndetësor, Ministria e Shëndetësisë kishte buxhet për blerjen e kompjuterëve por nuk kishte për blerjen dhe mirëmbajtjen e softuerit dhe shërbyesve (server) të bazave të të dhënave. Si përfundim të gjithë këta kompjuterë të rinj përfunduan në bodrumet e spitalit të Prishtinës. Rasti tjetër pak më i susksesshëm, sistemi kompjuterik i Doganave, ku u shpenzuan me miliona Euro për një sistem nga Estonia, dhe akoma vazhdojmë të paguajmë me miliona Euro për mirëmbajtje e tij. Ose tragjedia e fundit e KQZ-së ku serverat me Windows 2008 dështuan mu në mes të numërimit të rezultateve. Shkaku qesharak: shumë klikime në web faqe, edhe pse ai ishte një server i dedikuar për klikime. 

**Zgjidhja**

Të trija këto sisteme, dhe shumë projekte të tjera të ndërmarrura nga qeveritë lokale dhe ajo qëndrore, do të kishin pasur zgjidhje vendore, shumë më të lira, dhe të mirëmbajtura nga vetë profesionistët e Kosovës.

**Si mund t'i ndihmoj FLOSSK-u Kosovës?**

Anëtarët e FLOSSK-ut mund të angazhohet për krijimin e standareve të hapura të cilat do të zbatoheshin në projektet teknologjike nga institucionet e Republikës së Kosovës. Kosova ka adoptuar standarde të interoperabilitetit të vendosura nga BE-ja por këto janë të pamjaftueshme sepse kanë pësuar nga lobimi i gjigandëve softuerik. FLOSSK mund të rekomandoj të gjithë sistemin, apo eko-sistemin dhe gjen teknologjitë me kod të hapur që mund të zëvendësohen në projekt. Pastaj rekomandon edhe çfarë parimesh bazike të hapjes duhet të ketë specifikacioni për të evituar kapjen (lock-in) nga ndonjë furnitor.

Qeveria duhet të kuptojë epërsinë e kodit të hapur ndaj atij me kod të mbyllur. Teknologjia me kod të hapur lejon që një kompani të krijojë një produkt. Pastaj atë produkt insituticioni mund t'ia jap ndonjë kompanie tjetër për ta përmirësuar apo për zhvillim të mëtutjeshëm. Me këtë model nuk varemi nga një kompani e vetme për një përmirësim apo ndryshim të projektit.

Kursimet përmes FLOSS-it janë shumë të mëdha. Për shembull nëse ndojnë agjenci e qeverisë blen 30 shërbyes me nga 16 bërthama, vetëm licenca për Windows Server 2016 Standard 16 core do të kushtojë 900 Euro për copë. D.m.th softueri FLOSS do t'i kursente 27.000 EUR vetëm me përdorimin e Linux në vend të Windows. Por kjo është vetëm një shembull. Shembulli tjetër MS SQL Server Standard kushton 3,500 EUR për bërthamë, pra 16x30x3.500 = 728.000 Euro. Versioni Open Source apo me kod të hapur kushton 0 EUR.

**Përfitimi**

Spefikacionet teknike për teknologji të hapura u japin përparësi rishfrytëzimit të teknologjive ekzistuese dhe kuadrove dhe kompanive vendore që punojnë me kod të hapur.

Përfitimi këtu është se buxheti i Kosovës nuk duhet të shkojë jasht Kosovës, por mbetet në Kosovë te kompanitë të cilat kontribuojnë në ekonominë e Kosovës dhe krijojnë vende të reja pune si në fazën e krijimit të projekteve ashtu edhe vende të reja pune për fazën e mirëmbajtjs së projekteve.
Këto projekte shëndërrohen në prodhime dhe shërbime për shtetet tjera dhe vendosin Kosovë në listë bashkë me vendet e zhvlilluara të cilat eksportojnë teknologji. Në të njejtën kohë, edhe shërbimet për mirëmbajtjen e këtyre prodhimeve do të sjellin para në Kosovë.

Përfitimi tjetër është pavarsia teknologjike. FLOSS rritë profitin e kompanive vendore por në të njejtën kohë edhe zvogëlon dhe eventualisht eliminon varësinë nga teknologjitë e huajat me kod të mbyllur. Kjo i jap fleksibilitet qeverisë dhe kompanive vendore që të avancojnë vet produktet ekzistuese. 

**Shembuj suksesi**

www.KuMeVotu.info është një shërbim i krijuar në vitin 2014 i cili në kohën me ngarkesën më të madhe punoj ashtu siç është dizajnuar nga anëtarët e FLOSSK-ut. Ai sistem që nga 2014 është duke  punuar pa asnjë intervenim, dhe kështu do të punoje edhe për 20 vjet të ardhshme.

www.prishtinabuses.info është një sistem tjetër ku funksionaliteti, kualiteti dhe qëndrushmëria në pikpamje inxhinjerike, duke i përdorë të gjitha njohuritë e shkencave kopjuterike më moderne të implementuara në këtë server. Ky sistem është në funksion prej vitit 2010.

12 qershor 2017 në Nju Xherzi
Agron Selimaj - Anëtar i FLOSSK-ut
