---
layout: post
title: 'Projekti i Drupal në FLOSSK'
lang: sq
category: sq
author: "admin"
---
![alt](/flossk.org/img/blog/drupal-logo.jpg)
Përveq se FLOSSK vazhdon promovimin e këti CMS, ndër qështiet kryesore është se FLOSSK tanimë përdor këtë CMS.

	Drupal është nje open source Content Management Platform (CMS) I shkruajtur ne PHP, me licencë të hapur GPL që përdoret nga miliona websajte dhe aplikacione në botë.
	Ky CMS është ndërtuar, përdorur dhe perkrahur nga shumë komunitete dhe njerëz aktiv në teknologji, veqanërisht perkrahës te softuerit të lirë dhe Open Source.
	Një ndër sajtet më të njohura që është ndërtuar në Drupal është edhe sajti I vetë Qeverisë Amerikane (Shtepisë së Bardhe)http://www.whitehouse.gov/ mund ta vërtetoni këtu http://isthissitebuiltwithdrupal.com/

	Pse të Përdorni Drupal edhe ju ?
	Perdorni Drupal të ndërtoni qfardo blogu personal e madje edhe websajte shumë të avancuara ndërmarjesh e firmash të mëdha. Qindra add-ons module dhe dizajne ju lejojnë të ndertoni qfarëdo websajti që ju keni imagjinuar.
	Drupal është I lirë (pa pagesë), fleksibil, dhe vazhdimisht I modifikuar nga qindra mijëra pasionant nga mbarë bota, dhe me qindra e mijëra njerez dhe organizata përdorin Drupal, bashkohuni edhe ju !

	Vizitoni faqen kryesore të Drupal:http://drupal.org
