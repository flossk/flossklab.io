---
layout: post
title: 'A country list - good for all'
author: "agron"
---
If you are a web developer, pc software programmer, app developer, Linux distro packager you have probably heard many complaints from your users about you list of countries and country codes. Most of the complaints come from people not finding their country on the list. For example, Europe has changed a lot in the last two decades. Countries have dissolved and new ones were created. There are changes in Asia, Africa and in South America. The sad fact is that many web sites still list Yugoslavia in their list of countries. Which is not a bad thing because you might use Yugoslavia country code as key for other tables in your database, but not adding the countries is what frustrates people a lot. The irony for websites using old and hardcoded data is because websites are online all the time. They can use a web-service and update the list dynamically or periodically. Websites are not like pc programs or linux distribution which are designed as stand alone solutions. Another complaint is that multilingual websites, phone apps and pc applications show the country list in English only, regardless of what language the user has chosen.
