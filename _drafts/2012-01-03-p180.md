---
layout: post
title: 'The Launch Party of "Prishtina Buses" project'
author: "altin"
---
![alt](/flossk.org/img/blog/pb-map2.png)
Too many candidates in the campaigns promised an improvement of public transportation in Prishtina, but so much was said and so little was done in this direction, and what the municipality failed to do, some teenagers did it.
On 12 January 2012 at the office of UNICEF Innovations Lab Kosovo at 18:00PM will be held the official launch party of Prishtina Buses project.
The event will begin with a brief presentation about the project, which will also be continued with some discussions around it and the plans for the further development of this idea...
The event is open also on facebook and everyone is welcomed.
What is Prishtina Buses ?
	Prishtina Buses is a project which lies a very good idea developed and realized by our team made ​​up of FLOSSK members and our organizations fellows. This project will enable all citizens of Kosovo to watch online all the bus lines of Prishtina, schedules of departures, prices etc.
We all know how hard it was to get information about the bus lines that circulate in Prishtina, starting from the point when we stopped unknown people in the street and asked them about what we needed.
	Knowing that as the capital city, Prishtina has no information system on public bus transportation, but there are 16 bus lines operating in Prishtina and in the closest area, locals as well as internationals were very confused about their operation. This fact, together with our belief that people would use it more often if there is more information, was the main impetus to start this project.
How it was done ?
	After the approval of the project by UNICEF Innovations Lab, the first phase of the project began.
	We took each bus line in a team of two and traced it with a Garmin GPS tracking devices with which we wrote down all the routes and bus stops. These informations were easily transferred into a computer with a map software.
	Also, as on the bus, we got exact information from the personnel on the bus operation schedule.
After the Team had all the bus lines informations, the IT guys worked hard to create this website until it looked like now.
The long term objective of the Prishtina Busses project, which cost only around 500 Euros, is to contribute to the improvement of the quality of life in Prishtina and to support people to take buses more often rather than cars as well as to contribute to the decrease of pollution in the city.
We hope it will serve as an inspiration for public administration to carry out and support this kind of project in other towns in Kosovo.
We thank UNICEF Innovations Lab for their support and we encourage everyone with innovative ideas to make them real.
On the map you can find the UNICEF Innovations Lab office:

View Larger Map
