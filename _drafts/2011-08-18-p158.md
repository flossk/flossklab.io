---
layout: post
title: 'Lecture: The connection between innovation, hacking and new social movements'
author: "altin"
---
![alt](/flossk.org/img/blog/dan_thumb.jpg)
Organized by FLOSSK and UNICEF Innovations Lab Kosovo this Monday a lecture is being held on topics such as relationship between innovation, hacking and new social movements from Dan McQuillan.

	Dan McQuillan is a social network activist and winner of "Global Ideas Bank Social Innovations Award", lecture begins at 15:30 am in the premises of UNICEF Kosovo Innovations Lab.In the end Dan McQuillan is expected to discuss with the audience about subjects such as innovation, social networks, hacking, etc.

	The event is open to all and free, it's expected to have considerable number of participants who have already begun to confirm participation in the event created on Facebook.

	The laboratory is close to the UNICEF office in the location here:



	View larger map.
