---
layout: post
title: 'PhpDay in Prishtina'
author: "altin"
---
![alt](/flossk.org/img/blog/phpday.jpg)
FLOSSK in collaboration with UNICEF Innovations lab Kosovo held PHP Day  (meeting / event) on Tuesday 27 September 2011 in the UNICEF Innovations Lab in Prishtina
	The aim of the meeting was, gathering PHP developers from the country and was intended only for PHP development.
During the event despite the presentation that was expected to happen from Flamur Mavraj, attendees had the opportunity to discuss open topics with their colleagues and also meet each other...
More precisely the presentation contained the following topics:
	1. Basic PHP things
	2. Different structures and design Patterns
	3. OOP (object oriented programming)
	4. Frameworks (Zend Framework and symfony2)
	5. Doctrine2 and nosql databases
	6. Examples, and questions & answers.
There were snacks, drinks fun and more than 30 participants who seemed to like the event.
Have a look at the Gallery to see the event closely.
	And below is Flamur's Presentation:



			PHP Day at UNICEF Innovations Lab of Kosova



			View more presentations from Flamur Mavraj
