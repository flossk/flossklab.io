---
layout: post
title: 'Free Digital Society with Richard Stallman'
author: "admin"
---
![alt](/flossk.org/img/blog/rms.png)
With the initiative of FLOSSK on June 4, Richard Stallman, founder and president of the Free Software Foundation Richard Stallman visited Kosovo.
	During this visit RMS held a lecture titled A Free Digital Society, setting out what digital society is being created and illustrating the dangers facing it.
	This speech was held at the National Library of Kosovo and other meetings were also held with information technology enthusiasts in Prishtina and Gjakova.
Richard Matthew Stallman is an American  Free Software activist, hacker and software developer also known by the initials RMS.
Stallman established the GNU Project. He’s also recognized for his support of GNU/Linux and as president of the Free Software Foundation, which among other things developed the GNU General Public License.
Richard Stallman received many honors and recognitons for his work. Among many:

		1990: MacArthur Fellowship

		1991: Grace Murray Hopper Award

		1998: EFF Pioneer Award

		2001: Takeda Foundation Award
View the  FLOSSK Gallery to see photos from this event.
	Listen the lecture below:
